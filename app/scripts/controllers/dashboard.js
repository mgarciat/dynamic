'use strict';
/*global $ */

/**
 * @ngdoc function
 * @name dynamicApp.controller:DashboardCtrl
 * @description
 * # DashboardCtrl
 * Controller of the dynamicApp
 */
angular.module('dynamicApp')
  .controller('DashboardCtrl', function ($scope,customLogin,$location) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    $scope.user = customLogin.user;

    if(!$scope.user.logged){
      $location.path('/login'); 
    }

    $('.chart').easyPieChart({
        barColor : '#ffc20f',
        trackColor : '#eaeaea',
        lineWidth : '6'
    });
    

  });
