'use strict';

/**
 * @ngdoc function
 * @name dynamicApp.controller:TrainingCtrl
 * @description
 * # TrainingCtrl
 * Controller of the dynamicApp
 */
angular.module('dynamicApp')
  .controller('TrainingCtrl', function ($scope) {
    $scope.routines = [
      'Burpee 10',
      'Climber 30',
      'Squat 40'

    ];
  });
