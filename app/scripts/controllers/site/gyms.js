'use strict';



/**
 * @ngdoc function
 * @name dynamicApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the dynamicApp
 */
angular.module('dynamicApp')
  .controller('GymsCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];


  });
