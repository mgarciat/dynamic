'use strict';
/*global google,WOW*/

/**
 * @ngdoc function
 * @name dynamicApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the dynamicApp
 */

angular.module('dynamicApp')
  .controller('MainCtrl', function ($scope,bootstrapper) {

	
	bootstrapper();
	$scope.currentPage = 'dashboard';
	new WOW().init();

	var map;
	function initialize() {
	  var mapOptions = {
		scrollwheel: false,
		zoom: 8,
		center: new google.maps.LatLng(19.3907337,-99.1436126)
	  };
	  
	  setTimeout(function(){
		map = new google.maps.Map(document.getElementById('map-canvas'),
		  mapOptions);
		var styles = [
		  {
			stylers: [
			  { hue: '#00ffe6' },
			  { saturation: -100 }
			]
		  },{
			featureType: 'road',
			elementType: 'geometry',
			stylers: [
			  { lightness: 100 },
			  { visibility: 'simplified' }
			]
		  },{
			featureType: 'road',
			elementType: 'labels',
			stylers: [
			  { visibility: 'off' }
			]
		  }
		];
		
		var infowindow = new google.maps.InfoWindow({
		  content: '<h3>Gym name 1</h3><p>Esquina con Motolinia</p><p>5 de Mayo 35</p>'
		});

		var marker = new google.maps.Marker({
		  position: new google.maps.LatLng(19.3907337,-98.1436126),
		  map: map,
		  title: 'Gym numero 1',
		  icon: 'images/marker-pin.png'
		});

		var marker2 = new google.maps.Marker({
		  position: new google.maps.LatLng(19.3917337,-99.1491126),
		  map: map,
		  title: 'Gym numero 2',
		  icon: 'images/marker-pin.png'
		});

		var marker3 = new google.maps.Marker({
		  position: new google.maps.LatLng(19.3807337,-99.9886126),
		  map: map,
		  title: 'Gym numero 3',
		  icon: 'images/marker-pin.png'
		});

		google.maps.event.addListener(marker, 'click', function() {
			infowindow.open(map,marker);
		});

		google.maps.event.addListener(marker2, 'click', function() {
			infowindow.open(map,marker2);
		});

		google.maps.event.addListener(marker3, 'click', function() {
			infowindow.open(map,marker3);
		});

		map.setOptions({styles: styles});
		  
	  },900);

	}

	google.maps.event.addDomListener(window, 'load', initialize);

	

  });

