'use strict';
/*global Firebase,sweetAlert */

/**
 * @ngdoc function
 * @name dynamicApp.controller:DashboardProgramCtrl
 * @description
 * # DashboardProgramCtrl
 * Controller of the dynamicApp
 */
angular.module('dynamicApp')
  .controller('DashboardProgramCtrl', ['$scope','$firebaseArray','firebaseRef','$location',
	function ($scope,$firebaseArray,firebaseRef,$location) {

	firebaseRef.updateScope();
	$scope.programs  = firebaseRef.programs;
	$scope.refRoutines = firebaseRef.refRoutines;
	$scope.reverse = true;

	$scope.order = function(){
		$scope.reverse = !$scope.reverse;
	};

	$scope.changeView = function(view){
		$scope.currentView = view;
	};

	$scope.addProgram = function(){
		sweetAlert({
				title: 'Nombre del nuevo programa',
				type : 'input'
			},function(value){
				if(value){
					$scope.programs.$add({
						name : value, //$scope.routine.name,
						routines : {
						},
						date : Firebase.ServerValue.TIMESTAMP
					}).then(function(program){
						var id = program.key();
						sweetAlert('Programa Agregado');
						$location.path('/dashboard/programs/edit/'+id);
					});
				}
			});


		
	};
	
	$scope.changeEditable = function(id){
		$location.path('/dashboard/programs/edit/'+id);
	};

	$scope.removeProgram = function(programId){
		var program = firebaseRef.getRef('programs/' + programId);
		program.remove(function(){
			sweetAlert('Programa Eliminado');
		});
		
	};

	$scope.addRoutine = function(){
		$scope.refRoutines.child('routine1').set({
			name : 'routine1', //$scope.routine.name,
			excercises : {
				'-JqoYYDLBbxJogW7LhAi' : {
					'name' : 'squats',
					'reps' : 300
				},
				'-JqoYYDLBbxJogW7LhAj' : {
					'name' : 'pushups',
					'reps' : 100
				},
				'-JqoYYDLBbxJogW7LhAk' : {
					'name' : 'blasts',
					'reps' : 300
				},
				'-JqoYYDLBbxJogW7LhAl' : {
					'name' : 'punchs',
					'reps' : 900
				}

			},
			date : Firebase.ServerValue.TIMESTAMP
		});
	};
	

  }]).controller('DashboardProgramEditCtrl', ['$scope','$firebaseArray','$routeParams','firebaseRef',
	function ($scope,$firebaseArray,$routeParams,firebaseRef) {
		var routinesCount,
		expCount,
		obj,
		ref = firebaseRef.getRef();

		firebaseRef.updateScope();
		$scope.preDefRoutines  = firebaseRef.routines;
		$scope.refRoutines = firebaseRef.refRoutines;

		


		var fetchProgramData = function(){

			ref.child('programs/'+$routeParams.id).once('value',function(program){
				routinesCount = 0;
				obj = {};
				$scope.program = program.val();

				expCount = ($scope.program.routines) ? Object.keys($scope.program.routines).length : 0;

				if(expCount > 0){
					angular.forEach($scope.program.routines,function(r,val){

						ref.child('routines/' + val).once('value',function(routine){
							var rtn = routine.val() || '';

							obj[routine.key()] = rtn;

							//angular.extend(obj,rtn);
							if(++routinesCount === expCount  ){
								setTimeout(function() {
									$scope.$apply(function(){
										console.log(obj);
										$scope.routines = obj;
										console.log('fetched routines');
									});	
								}, 300);
								
							}
						});
					});
				}

			});

		};


	$scope.updateProgram = function(){
		var routinesCount = ($scope.routines) ? Object.keys($scope.routines).length : 0,
			iter = 0;

		var onSaved = function(){
			ref.child('programs/'+$routeParams.id).set($scope.program,function(){
				fetchProgramData();
				sweetAlert('Guardado');
			});
		};

		console.log($scope.routines);
		//save routines
		angular.forEach($scope.routines,function(r,val){
			ref.child('routines/' + val).set(r);
			if(++iter === routinesCount){
				onSaved();
			}

		});

		
	};

	$scope.addExcercise = function(routineId,pause){
		

		var refExcercices = firebaseRef.getRef('routines/' + routineId + '/excercises');
		
		if(typeof pause === 'undefined' || pause !== true){
			refExcercices.push({
				'name':'Nombre del Ejercicio',
				'reps' : 0
			},function(){
				fetchProgramData();
			});
		}else{
			refExcercices.push({
				'name':'Pausa',
				'tiempo' : '10 S',
				'pause' : true
			},function(){
				fetchProgramData();
			});
		}
	};



	$scope.removeExcercise = function(excerciseId,routineName){
		var rtnName = angular.fromJson(angular.toJson(routineName.name)),
		refExcercice = firebaseRef.getRef('routines/' + rtnName + '/excercises/' + excerciseId);
		refExcercice.set(null,function(){
			fetchProgramData();
		});
	};


	$scope.addRoutine = function(){
			var newRoutine;

			sweetAlert({
				title: 'Nombre de la nueva rutina',
				type : 'input'
			},function(value){
				if(value){
					newRoutine = $scope.refRoutines.push({
						name : value, //$scope.routine.name,
						excercises : {
						},
						date : Firebase.ServerValue.TIMESTAMP
					});

					newRoutine.child('excercises').push({
						'name':'Nombre del Ejercicio',
						'reps' : 0
					});

					if(typeof $scope.program.routines === 'undefined'){
						$scope.program.routines = {};
						$scope.routines = {};
					}
					$scope.program.routines[newRoutine.key()] = 'true';
					//$scope.routines[newRoutine.key()] = $firebaseArray(newRoutine);
					$scope.updateProgram();
				}
			});


	};

	$scope.deleteRoutine = function(routineId){
		console.log(routineId);
		sweetAlert({
			title: 'Desea eliminar esta rutina?',
			text: 'Por favor confirme',
			type: 'warning',
			showCancelButton: true,
			closeOnConfirm: false,
		},function(value){
			if(value){
				delete $scope.program.routines[routineId];
				$scope.updateProgram();
				var routine = firebaseRef.getRef('routines/' + routineId);
				routine.remove(function(){
					sweetAlert('Rutina Eliminada');
				});
			}
		});
	};

	//initialize fetching the program data
	fetchProgramData();


  }]);
