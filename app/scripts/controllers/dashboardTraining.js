'use strict';
/*global Firebase,sweetAlert */

/**
 * @ngdoc function
 * @name dynamicApp.controller:DashboardTrainingCtrl
 * @description
 * # DashboardTrainingCtrl
 * Controller of the dynamicApp
 */
angular.module('dynamicApp')
  .controller('DashboardTrainingCtrl', ['$scope','$firebaseArray','firebaseUrl',
  	function ($scope,$firebaseArray,firebaseUrl) {
    
    var ref = new Firebase(firebaseUrl + '/routines');
	$scope.routines = $firebaseArray(ref);

	$scope.getRoutines = function(){

	};

	$scope.addRoutine = function(){
		$scope.routines.$add({
			name : 'Pushups', //$scope.routine.name,
			reps : 30, //$scope.routine.reps,
			date : Firebase.ServerValue.TIMESTAMP
		}).then(function(){
			//$scope.routine.name = ''; 
			//$scope.routine.reps = ''; 

			sweetAlert('Your message has been sent!');
		});
		
	};
    
  }]);
