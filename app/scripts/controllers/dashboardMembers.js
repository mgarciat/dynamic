'use strict';
/*global Firebase,sweetAlert */

/**
 * @ngdoc function
 * @name dynamicApp.controller:DashboardMembersCtrl
 * @description
 * # DashboardMembersCtrl
 * Controller of the dynamicApp
 */
angular.module('dynamicApp')
  .controller('DashboardMembersCtrl', ['$scope','$firebaseArray','firebaseUrl',
  function ($scope,$firebaseArray,firebaseUrl) {
    
    var ref = new Firebase(firebaseUrl + '/members');
    $scope.members = $firebaseArray(ref);
    $scope.currentView = 'lista';


    $scope.newMember = {
		date : Firebase.ServerValue.TIMESTAMP,
		name : '', 
		age : 0,
		sex : 'M',
		programs : {
		},
		profile : {
			'weight' : 89

		}

	};
    
    $scope.saveMember = function(){
    	$scope.members.$add($scope.newMember).then(function(){
			sweetAlert('Socio creado');
		});
    };

	$scope.changeView = function(view){
		$scope.currentView = view;
	};

    
  }])
  .controller('DashboardMembersEditCtrl', ['$scope','$firebaseObject','$routeParams','firebaseRef',
  function ($scope,$firebaseObject,$routeParams,firebaseRef) {
    
    var ref = firebaseRef.getRef();
    $scope.allPrograms =  $firebaseObject(ref.child('programs/'));
    console.log($scope.allPrograms);

    ref.child('members/' + $routeParams.id).once('value',function(
      member){
        var obj = {},
            counter = 0,
            total = 0;

        $scope.member = member.val();
        total = ($scope.member.programs) ? Object.keys($scope.member.programs).length : 0;

          angular.forEach($scope.member.programs,function(m,val){
            ref.child('programs/' + val).once('value',function(prg){
              var program = prg.val();
              obj[program.name] = program;
                
              if(++counter === total){
                setTimeout(function() {
                  $scope.$apply(function(){
                    $scope.programs = obj;  
                  });
                },300);

              }
            });
          });
        
    });

    $scope.addProgram = function(){

    };


    $scope.addProfileFeature = function(){
    	//profile = { '' : '' };
    	//console.log($scope.member.profile);

    };
    
  }]);
