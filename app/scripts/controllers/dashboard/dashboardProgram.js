'use strict';
/*global Firebase,sweetAlert */

/**
 * @ngdoc function
 * @name dynamicApp.controller:DashboardProgramCtrl
 * @description
 * # DashboardProgramCtrl
 * Controller of the dynamicApp
 */
angular.module('dynamicApp')
  .controller('DashboardProgramCtrl', ['$scope','$firebaseArray','programLoader','$location',
	function ($scope,$firebaseArray,programLoader,$location) {

	$scope.programs  = programLoader.loadAllPrograms();
	$scope.reverse = true;

	$scope.order = function(){
		$scope.reverse = !$scope.reverse;
	};

	$scope.changeView = function(view){
		$scope.currentView = view;
	};

	$scope.addProgram = function(){
		sweetAlert({
				title: 'Nombre del nuevo programa',
				type : 'input'
			},function(value){
				if(value){
					$scope.programs.$add({
						name : value, //$scope.routine.name,
						routines : {
						},
						date : Firebase.ServerValue.TIMESTAMP
					}).then(function(program){
						var id = program.key();
						sweetAlert('Programa Agregado');
						$location.path('/dashboard/programs/edit/'+id);
					});
				}
			});


		
	};
	
	$scope.changeEditable = function(id){
		$location.path('/dashboard/programs/edit/'+id);
	};

	$scope.removeProgram = function(programId){
		var program = programLoader.loadProgramById(programId);

		program.remove(function(){
			sweetAlert('Programa Eliminado');
		});
		
	};

	$scope.addRoutine = function(){
		$scope.refRoutines.child('routine1').set({
			name : 'routine1', //$scope.routine.name,
			excercises : {
				'-JqoYYDLBbxJogW7LhAi' : {
					'name' : 'squats',
					'reps' : 300
				},
				'-JqoYYDLBbxJogW7LhAj' : {
					'name' : 'pushups',
					'reps' : 100
				},
				'-JqoYYDLBbxJogW7LhAk' : {
					'name' : 'blasts',
					'reps' : 300
				},
				'-JqoYYDLBbxJogW7LhAl' : {
					'name' : 'punchs',
					'reps' : 900
				}

			},
			date : Firebase.ServerValue.TIMESTAMP
		});
	};
	

  }]).controller('DashboardProgramEditCtrl', [
  '$scope','$firebaseArray','$routeParams','programLoader',
	function ($scope,$firebaseArray,$routeParams,programLoader) {
		var routinesCount,
		expCount,
		obj,
		iter = 0;
		$scope.program = programLoader.loadProgramById($routeParams.id);
		$scope.routines = programLoader.loadRoutinesByProgram();

		var fetchProgramData = function(){
		};

		var saveRoutines = function(){
			angular.forEach($scope.routines,function(r,val){
				r.$save().then(function(ref){
					iter++;
				})

			});
		}

		$scope.updateProgram = function(){
			var routinesCount = ($scope.routines) ? Object.keys($scope.routines).length : 0,
				iter = 0;

			$scope.program.$save().then(function(ref){
				sweetAlert('Guardado');
				saveRoutines();
			},function(error){
				console.log("error saving " + error);
			});


			/*

			var onSaved = function(){
				ref.child('programs/'+$routeParams.id).set($scope.program,function(){
					fetchProgramData();
					sweetAlert('Guardado');
				});
			};

			console.log($scope.routines);
			//save routines

			*/

		};

	$scope.addExcercise = function(routineId,pause){
		
		var exercises = programLoader.loadExercisesByRoutineId(routineId);
		
		if(typeof pause === 'undefined' || pause !== true){
			exercises.$loaded().then(function(){
				exercises.$add({
					'name':'Nombre del Ejercicio',
					'reps' : 0
				});	
			});
		}else{
			exercises.$loaded().then(function(){
				exercises.$add({
					'name':'Pausa',
					'tiempo' : '10 S',
					'pause' : true
				});
			});
		}
	};



	$scope.removeExcercise = function(excerciseId,routineName){
		var rtnName = angular.fromJson(angular.toJson(routineName.name)),
		removeExerciseObj = programLoader.loadExercise(rtnName,excerciseId);


		removeExerciseObj.$loaded().then(function(){
			console.log(removeExerciseObj);
			removeExerciseObj.$remove().then(function(ref){
				sweetAlert('Ejercicio Eliminado');
			},function(){
				console.log("error removiendo");
			});
		});
		
	};


	$scope.addRoutine = function(){
			var newRoutine;

			sweetAlert({
				title: 'Nombre de la nueva rutina',
				type : 'input'
			},function(value){
				if(value){
					newRoutine = $scope.refRoutines.push({
						name : value, //$scope.routine.name,
						excercises : {
						},
						date : Firebase.ServerValue.TIMESTAMP
					});

					newRoutine.child('excercises').push({
						'name':'Nombre del Ejercicio',
						'reps' : 0
					});

					console.log($scope.program);

					if(typeof $scope.program.routines === 'undefined'){
						$scope.program.routines = {};
						//$scope.routines = {};
					}
					$scope.program.routines[newRoutine.key()] = 'true';
					//$scope.routines[newRoutine.key()] = $firebaseArray(newRoutine);
					$scope.updateProgram();
				}
			});


	};

	$scope.deleteRoutine = function(routineId){
		console.log(routineId);
		sweetAlert({
			title: 'Desea eliminar esta rutina?',
			text: 'Por favor confirme',
			type: 'warning',
			showCancelButton: true,
			closeOnConfirm: false,
		},function(value){
			if(value){
				delete $scope.program.routines[routineId];
				$scope.updateProgram();
				var routine = firebaseRef.getRef('routines/' + routineId);
				routine.remove(function(){
					sweetAlert('Rutina Eliminada');
				});
			}
		});
	};

	//initialize fetching the program data
	fetchProgramData();


  }]);
