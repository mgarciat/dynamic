'use strict';
/*global Firebase,sweetAlert */

/**
 * @ngdoc function
 * @name dynamicApp.controller:DashboardTrainingCtrl
 * @description
 * # DashboardTrainingCtrl
 * Controller of the dynamicApp
 */
angular.module('dynamicApp')
  .controller('DashboardDietCtrl', ['$scope','$firebaseArray','firebaseUrl','$location',
  	function ($scope,$firebaseArray,firebaseUrl,$location) {
    
    var ref = new Firebase( firebaseUrl +  '/diets');
	$scope.diets = $firebaseArray(ref);

	$scope.pushDiet = function(){
		$scope.diets.$add({
			name : 'Semanal', //$scope.routine.name,
			dias : {
				lunes : {
					desayuno : 'Vaso de leche, pan tostado',
					tentempie : 'Vaso de leche, pan tostado',
					comida : 'Vaso de leche, pan tostado',
					merienda : 'Vaso de leche, pan tostado',
					cena : 'Vaso de leche, pan tostado'
				},
				martes : {
					desayuno : 'Vaso de leche, pan tostado',
					tentempie : 'Vaso de leche, pan tostado',
					comida : 'Vaso de leche, pan tostado',
					merienda : 'Vaso de leche, pan tostado',
					cena : 'Vaso de leche, pan tostado'
				},
				miercoles : {
					desayuno : 'Vaso de leche, pan tostado',
					tentempie : 'Vaso de leche, pan tostado',
					comida : 'Vaso de leche, pan tostado',
					merienda : 'Vaso de leche, pan tostado',
					cena : 'Vaso de leche, pan tostado'
				},
				jueves : {
					desayuno : 'Vaso de leche, pan tostado',
					tentempie : 'Vaso de leche, pan tostado',
					comida : 'Vaso de leche, pan tostado',
					merienda : 'Vaso de leche, pan tostado',
					cena : 'Vaso de leche, pan tostado'
				},
				viernes : {
					desayuno : 'Vaso de leche, pan tostado',
					tentempie : 'Vaso de leche, pan tostado',
					comida : 'Vaso de leche, pan tostado',
					merienda : 'Vaso de leche, pan tostado',
					cena : 'Vaso de leche, pan tostado'
				},
				sabado : {
					desayuno : 'Vaso de leche, pan tostado',
					tentempie : 'Vaso de leche, pan tostado',
					comida : 'Vaso de leche, pan tostado',
					merienda : 'Vaso de leche, pan tostado',
					cena : 'Vaso de leche, pan tostado'
				}
				
			},
			date : Firebase.ServerValue.TIMESTAMP
		}).then(function(){
			//$scope.routine.name = ''; 
			//$scope.routine.reps = ''; 

			sweetAlert('Your message has been sent!');
		});
	};

	$scope.changeEditable = function(id){
		$location.path('/dashboard/diets/edit/'+id);
	};


	$scope.getDiet = function(){

	};

	$scope.addDiet = function(){
		$scope.pushDiet();

		
	};
	$scope.removeDiet = function(id){
		console.log(id);

		var ref = new Firebase( firebaseUrl +  '/diets/' + id);
		ref.remove();
		


	};

	$scope.editDiet = function(id){
		console.log('editing Id ' + id);
		
	};


	
    
  }])
.controller('DashboardDietEditCtrl', ['$scope','$firebaseObject','firebaseUrl','$location','$routeParams',
  	function ($scope,$firebaseObject,firebaseUrl,$location,$routeParams) {
	var ref = new Firebase( firebaseUrl +  '/diets/' + $routeParams.id);
	$scope.diet = $firebaseObject(ref);


  	}]);

