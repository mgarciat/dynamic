'use strict';
/*global Firebase */

/**
 * @ngdoc function
 * @name dynamicApp.controller:DashboardTrainingCtrl
 * @description
 * # DashboardTrainingCtrl
 * Controller of the dynamicApp
 */
angular.module('dynamicApp')
  .controller('DashboardGymsCtrl', ['$scope','$firebaseArray','firebaseUrl',
	function ($scope,$firebaseArray,firebaseUrl) {
	var ref = new Firebase( firebaseUrl +  '/gyms');
	$scope.gyms = $firebaseArray(ref);

	var addGyms = function(){
		$scope.gyms.$add({
			'name' : 'Energy Fitness',
			'type' : 'Weight & Fitness',
			'location' : '19.3204968,-99.1526134',
			'sucursals' : {
				'Sante Fe' : {
					'location' : '19.3204968,-99.1526134',
					'schedule' : 'Lunes a Sabado, 6 am - 11 pm / Domingo 9 am - 2pm',
					'state' : 'DF',
					'amenities' : {
						'free-weight' : true,
						'cardio' : true,
						'crossfit' : true,
						'relaxation' : true,
						'vestidores' : true,
						'piscina' : true,
						'sauna' : true,
						'vapor' : true,
						'personal-trainer' : true
					}
				},
				'Macroplaza' : {
					'location' : '19.3204968,-99.1526134',
					'schedule' : 'Lunes a Sabado, 6 am - 11 pm / Domingo 9 am - 2pm',
					'state' : 'DF',
					'amenities' : {
						'free-weight' : true,
						'cardio' : true,
						'crossfit' : true,
						'relaxation' : true,
						'vestidores' : true,
						'piscina' : true,
						'sauna' : true,
						'vapor' : true,
						'personal-trainer' : true
					}
				},
				'Interlomas' : {
					'location' : '19.3204968,-99.1526134',
					'schedule' : 'Lunes a Sabado, 6 am - 11 pm / Domingo 9 am - 2pm',
					'amenities' : {
						'free-weight' : true,
						'cardio' : true,
						'crossfit' : true,
						'relaxation' : true,
						'vestidores' : true,
						'piscina' : true,
						'sauna' : true,
						'vapor' : true,
						'personal-trainer' : true
					}
				},
				'Polanco' : {
					'location' : '19.3204968,-99.1526134',
					'schedule' : 'Lunes a Sabado, 6 am - 11 pm / Domingo 9 am - 2pm',
					'amenities' : {
						'free-weight' : true,
						'cardio' : true,
						'crossfit' : true,
						'relaxation' : true,
						'vestidores' : true,
						'piscina' : true,
						'sauna' : true,
						'vapor' : true,
						'personal-trainer' : true
					}
				}

			}
		});
	};

}]);
