'use strict';


/**
 * @ngdoc function
 * @name dynamicApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the dynamicApp
 */
angular.module('dynamicApp')
  .controller('LoginCtrl', ['$scope','customLogin','$location','$auth',
  	function ($scope,customLogin,$location,$auth) {
    
        $scope.user = customLogin.user;
        $scope.login = customLogin.login;
        $scope.logout = customLogin.logout;
        $scope.newUser = customLogin.newUser;
        $scope.register = customLogin.register;

        if($scope.user.logged){
            $location.path('/dashboard');
        }

        $scope.authenticate = function(provider) {
          $auth.authenticate(provider);
        };

        $scope.loginCallback = function(){
            $scope.$apply(function(){
                $location.url('/dashboard');
            });
        };

        $scope.logoutCallback = function(){
                $location.path('/');
        };

        

  }]);
