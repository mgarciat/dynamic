'use strict';

/**
 * @ngdoc overview
 * @name dynamicApp
 * @description
 * # dynamicApp
 *
 * Main module of the application.
 */
angular
	.module('dynamicApp', [
		'ngAnimate',
		'ngCookies',
		'ngResource',
		'ngRoute',
		'ngSanitize',
		'ngTouch',
		'firebase',
		'ui.bootstrap',
		'satellizer'
	])
	.value('firebaseUrl','https://dynamic.firebaseio.com/')
	.config(function ($routeProvider,$authProvider) {
		$authProvider.facebook({
			clientId: '872743519479382',
			url: '/login',
		});

		$routeProvider
			.when('/', {
				templateUrl: 'views/main.html',
				controller: 'MainCtrl',
				header : 'views/header.html'
			})

			/*
				SITE
			*/

			.when('/about', {
				templateUrl: 'views/site/about.html',
				controller: 'AboutCtrl',
				header : 'views/header.html'
			})
			.when('/training', {
				templateUrl: 'views/site/training.html',
				controller: 'TrainingCtrl',
				header : 'views/header.html'
			})
			.when('/personalized', {
				templateUrl: 'views/site/personalized.html',
				controller: 'PersonalizedCtrl',
				header : 'views/header.html'
			})
			.when('/gyms', {
				templateUrl: 'views/site/gyms.html',
				controller: 'GymsCtrl',
				header : 'views/header.html'
			})
			.when('/openair', {
				templateUrl: 'views/site/openair.html',
				controller: 'OpenAirCtrl',
				header : 'views/header.html'
			})


			/*
				DASHBOARD
			*/

			.when('/login', {
				templateUrl: 'views/login.html',
				controller: 'LoginCtrl',
				header : 'views/dashboard/header.html'
			})
			.when('/dashboard', {
				templateUrl: 'views/dashboard.html',
				controller: 'DashboardCtrl',
				header : 'views/dashboard/header.html'
			})
			.when('/dashboard/programs', {
				templateUrl: 'views/dashboard/program.html',
				controller: 'DashboardProgramCtrl',
				header : 'views/dashboard/header.html'
			})
			.when('/dashboard/programs/edit/:id', {
				templateUrl: 'views/dashboard/programEdit.html',
				controller: 'DashboardProgramEditCtrl',
				header : 'views/dashboard/header.html'
			})
			.when('/dashboard/training', {
				templateUrl: 'views/dashboard/training.html',
				controller: 'DashboardTrainingCtrl',
				header : 'views/dashboard/header.html'
			})
			.when('/dashboard/members', {
				templateUrl: 'views/dashboard/members.html',
				controller: 'DashboardMembersCtrl',
				header : 'views/dashboard/header.html'
			})
			.when('/dashboard/members/edit/:id', {
				templateUrl: 'views/dashboard/membersEdit.html',
				controller: 'DashboardMembersEditCtrl',
				header : 'views/dashboard/header.html'
			})
			.when('/dashboard/diets', {
				templateUrl: 'views/dashboard/diet.html',
				controller: 'DashboardDietCtrl',
				header : 'views/dashboard/header.html'
			})
			.when('/dashboard/diets/edit/:id', {
				templateUrl: 'views/dashboard/dietEdit.html',
				controller: 'DashboardDietEditCtrl',
				header : 'views/dashboard/header.html'
			})

			.when('/dashboard/reports', {
				templateUrl: 'views/dashboard/reports.html',
				controller: 'DashboardReportsCtrl',
				header : 'views/dashboard/header.html'
			})

			.when('/dashboard/gyms', {
				templateUrl: 'views/dashboard/gyms.html',
				controller: 'DashboardGymsCtrl',
				header : 'views/dashboard/header.html'
			})

			.when('/dashboard/openair', {
				templateUrl: 'views/dashboard/openair.html',
				controller: 'DashboardOpenairCtrl',
				header : 'views/dashboard/header.html'
			})

			.when('/dashboard/personalized', {
				templateUrl: 'views/dashboard/personalized.html',
				controller: 'DashboardPersonalizedCtrl',
				header : 'views/dashboard/header.html'
			})

			.when('/dashboard/config', {
				templateUrl: 'views/dashboard/config.html',
				controller: 'DashboardConfigCtrl',
				header : 'views/dashboard/header.html'
			})

			.when('/dashboard/connect', {
				templateUrl: 'views/dashboard/connect.html',
				controller: 'DashboardConnectCtrl',
				header : 'views/dashboard/header.html'
			})
			.otherwise({
				redirectTo: '/'
			});
	});


	//map


