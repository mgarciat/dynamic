'use strict';
/*global $,Firebase,sweetAlert */

angular.module('dynamicApp')
.service('bootstrapper', function(){

    var docElem = document.documentElement,
		header = $( '.navbar-default' ),
		didScroll = false,
		changeHeaderOn = 150;

	function init() {
		window.addEventListener( 'scroll', function(  ) {
			if( !didScroll ) {
				didScroll = true;
				setTimeout( scrollPage, 250 );
			}
		}, false );
		
	}

	function scrollPage() {
		var sy = scrollY();
		if ( sy >= changeHeaderOn ) {
			header.addClass('navbar-shrink' );
		}
		else {
			header.removeClass('navbar-shrink' );
		}
		didScroll = false;
	}

	function scrollY() {
		return window.pageYOffset || docElem.scrollTop;
	}

    return function(){
    	init();	
    };

}).service('customLogin', ['firebaseUrl', function(firebaseUrl){
	var ref = new Firebase(firebaseUrl),
	user = {};

	user.logged = false;

	function login(user,callback){

		if(user.email !== ''){
			ref.authWithPassword({
			  	email : user.email,
	    		password : user.password
			}, function(error, authData) {
			  if (error) {
			    sweetAlert('Login Failed! ' + error);
			  } else {
	            user.logged = true;
	            user.authUser = authData.password.email;
			    callback();
			  }
			},{
				remember : 'sessiononly'
			});

		}else{
			sweetAlert('Introduce un correo valido');
		}


    }

    function logout(user,callback){
    	user.logged = false;
    	callback();
    }
    

    function newUser(){
    	ref.createUser({
    		email : user.email,
    		password : user.password
    	},function(error,userData){
    		if(error){
    			sweetAlert('Error creando usuario: ' + error);
    		}else{
    			sweetAlert('Usuario creado exitosamente' + userData.uid);
    		}
    	});
    }

    function register(){
    }

    
    return {
    	login : login,
    	logout : logout,
    	newUser : newUser,
    	register : register,
    	user : user


    };

}])
.factory('_', ['$window', function($window) {
  return $window._; // assumes underscore has already been loaded on the page
}])
.factory('firebaseRef',['$firebaseArray','firebaseUrl','$firebaseObject',function($firebaseArray,firebaseUrl,$firebaseObject){
	var service = {};
	service.getRef = function(hash){
		var hs = hash || '';
		return new Firebase(firebaseUrl+hs);
	};

	service.getArray = function(hash){
		return $firebaseArray(service.getRef(hash));
	};

	service.getObject = function(hash){
		return $firebaseObject(service.getRef(hash));
	};

	var init = function(){

	};

	init();
	return service;
}])
.factory('programLoader',['firebaseRef',function(firebaseRef){
	var service = {},
		program = {},
		
		programs = [],
		routines = [],
		exercises = [];

	service.loadAllPrograms = function(){
		programs = firebaseRef.getArray('/programs');
		return programs;
	};
	
	service.loadProgramById = function(id){
		program = firebaseRef.getObject('/programs/' + id);
		return program;
	};

	service.loadRoutineById = function(id){
		return firebaseRef.getObject('/routines/' + id);
	};

	service.loadRoutinesByProgram = function(){
		program.$loaded().then(function(){
			angular.forEach(program.routines,function(value,id){
				routines.push(service.loadRoutineById(id));
			});
		});
		return routines;
	};

	service.loadExercisesByRoutineId = function(id){
		exercises = firebaseRef.getArray('routines/' + id + '/excercises')
		return exercises;
	};

	service.loadExercise = function(rtnName,exerciseId){
		var exercise = firebaseRef.getObject('routines/' + rtnName + '/excercises/' + exerciseId);
		return exercise;
	}

	var init = function(){

	};
	init();
	return service;

}]);