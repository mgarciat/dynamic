'use strict';

/**
 * @ngdoc overview
 * @name dynamicApp
 * @description
 * # dynamicApp
 *
 * Main module of the application.
 */
angular.module('dynamicApp')
.controller('RootController',['$scope',function($scope){

	$scope.$on('$routeChangeSuccess', function (e, current,previous) {
    	$scope.currentRoute = current;
  	});

}]);
