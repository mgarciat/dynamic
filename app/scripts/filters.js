'use strict';
angular.module('dynamicApp').filter('nospace', function () {
    return function (value) {
        return (!value) ? '' : value.replace(/ /g, '');
    };
})
.filter('objSize',function(){
	var assocArraySize = function(obj) {
	    // http://stackoverflow.com/a/6700/11236
	    var size = 0, key;
	    for (key in obj) {
	        if (obj.hasOwnProperty(key)) {
	        	size++;
	        }
	    }
	    return size;
	};
	return assocArraySize;
}).filter('tableRoutine',function(){
	return function(input){
		input = input || '';
		var out = '';
	
		angular.forEach(input,function(value){

			out += value.name + ' - ' + value.reps + '      \n';
		});

		return out;

	};
});