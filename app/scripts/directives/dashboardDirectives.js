'use strict';

/**
 * @ngdoc function
 * @name dynamicApp.controller:DashboardProgramCtrl
 * @description
 * # DashboardProgramCtrl
 * Controller of the dynamicApp
 */

angular.module('dynamicApp')
.directive('programedit',function(){
	return {
		restrict : 'E',
		scope : {
			name : '@',
			routines : '@'
		},
		templateUrl: 'views/dashboard/programEdit.html',
		//link : function($scope){	
			//console.log($scope);
			//$scope.routines = angular.fromJson(attributes.routines);
			//console.log($scope);

		//}
	};
})
.directive('preloader',function(){
	return {
		restrict : 'E',
		templateUrl : 'views/partial/loader.html',
		controller : function(){
			console.log(this);

			this.kill = function(){

			};
		}
	};
})
.directive('routineinputs',function(){
	return {
		restrict : 'E',
		templateUrl : 'views/dashboard/routineInputs.html',
	};
})
.directive('dietView', ['$filter','$firebaseArray',function($filter,$firebaseArray){
	return {
		restrict : 'AE',
		templateUrl : 'views/partials/dietView.html',
		transclude : true,
		controller : function($scope){
			var daysOrder = $scope.daysOrder = ['lunes','martes','miercoles','jueves','viernes','sabado','domingo'],
				timeOrder = ['desayuno','tentempie','comida','merienda','cena'];

			function updateInputByOrder(input,order){
					var orderedObject = {},
					size = $filter('objSize')(input);

					for(var iter = 0; iter < size; iter++ ){
						orderedObject[order[iter]] = input[order[iter]];
					}
					return orderedObject;
			}

			$scope.$watch('diet',function(newValue,oldValue){
				newValue.dias = updateInputByOrder(newValue.dias,daysOrder);
				for(var i in newValue.dias){
					newValue.dias[i] = updateInputByOrder(newValue.dias[i],timeOrder);
				}
			});

			$scope.removeDiet = function(){
				
			};


		}

	};
}]);